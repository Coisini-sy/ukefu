package com.ukefu.webim.web.handler.admin.config;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.SystemUpdateconRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.SystemUpdatecon;

@Controller
@RequestMapping("/admin/config/update")
public class SystemUpdateconController extends Handler{

	@Autowired
	private SystemUpdateconRepository systemUpdateconRes ;
	
	@RequestMapping("/index")
    @Menu(type = "admin" , subtype = "config" , admin = true)
    public ModelAndView index(ModelMap map , HttpServletRequest request ) throws SQLException {
		List<SystemUpdatecon> systemUpdateconList = systemUpdateconRes.findByOrgi(super.getOrgi(request)) ;
		SystemUpdatecon systemUpdatecon = null ;
		if (systemUpdateconList == null || systemUpdateconList.size()==0) {
			systemUpdatecon = new SystemUpdatecon();
			systemUpdatecon.setOrgi(super.getOrgi(request));
			systemUpdateconRes.save(systemUpdatecon) ;
		}else {
			systemUpdatecon = systemUpdateconList.get(0) ;
		}
		map.addAttribute("systemUpdatecon",systemUpdatecon) ;
		return request(super.createAdminTempletResponse("/admin/updatecon/index"));
	}
	
	@RequestMapping("/save")
    @Menu(type = "admin" , subtype = "config" , admin = true)
    public ModelAndView save(ModelMap map , HttpServletRequest request , @Valid SystemUpdatecon systemUpdatecon) throws SQLException {
		if (systemUpdatecon!=null && !StringUtils.isBlank(systemUpdatecon.getId())) {
			SystemUpdatecon config = systemUpdateconRes.findByIdAndOrgi(systemUpdatecon.getId(),super.getOrgi(request)) ;
			if (config != null) {
				config.setSchedule(systemUpdatecon.isSchedule());
				config.setScheduletimes(systemUpdatecon.getScheduletimes());
				systemUpdateconRes.save(config) ;
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/admin/config/update/index"));
	}
}
